<?php

if (! defined('CMS_VERSION')) exit;

$uid = cmsms()->test_state(CmsApp::STATE_INSTALL) ? 1 : get_userid();

# Setup _authenticate template
try {
    $summary_template_type = new CmsLayoutTemplateType();
    $summary_template_type->set_originator($this->GetName());
    $summary_template_type->set_name('authenticate');
    $summary_template_type->set_dflt_flag(TRUE);
    $summary_template_type->set_lang_callback('SmitAuth::page_type_lang_callback');
    $summary_template_type->set_content_callback('SmitAuth::reset_page_type_defaults');
    $summary_template_type->set_help_callback('SmitAuth::template_help_callback');
    $summary_template_type->reset_content_to_factory();
    $summary_template_type->save();
} catch (CmsException $e) {
    debug_to_log(__FILE__ . ':' . __LINE__ . ' ' . $e->GetMessage());
    audit('', $this->GetName(), 'Installation Error: ' . $e->GetMessage());
}

try {
    $fn = __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . '_authenticate.tpl';
    if (file_exists($fn)) {
        $template = @file_get_contents($fn);
        $tpl = new CmsLayoutTemplate();
        $tpl->set_name('_authenticate');
        $tpl->set_owner($uid);
        $tpl->set_content($template);
        $tpl->set_type($summary_template_type);
        $tpl->set_type_dflt(TRUE);
        $tpl->save();
    }
} catch (CmsException $e) {
    // log it
    debug_to_log(__FILE__ . ':' . __LINE__ . ' ' . $e->GetMessage());
    audit('', $this->GetName(), 'Installation Error: ' . $e->GetMessage());
}
