<?php

use SMIT\SDK\Auth\Models\UserModel;

if (! defined('CMS_VERSION')) exit;

if ($this->getPreference('enabled') !== '1') {
    return;
}

function enabled_on_page_by_alias($alias) {
	$hierarchy = cmsms()->GetHierarchyManager();
	
	if (empty($alias)) {
		$alias = \cms_utils::get_current_alias();
	}

	$stack = [];
	$node = $hierarchy->find_by_tag('alias', $alias);
	while($node && $node->get_tag('id') > 0)  {
		$stack[] = $node;
		$node = $node->getParent();
	}

	if(count($stack) !== 0) {
		return $stack[count($stack)-1]->getContent()->GetPropertyValue('enabled_on_page');
	}

	return null;
}

$client = (new Auth())->client;
$active = (bool) enabled_on_page_by_alias(\cms_utils::get_current_alias());
$isLoggedIn = $client->isLoggedIn();

if ($active && !$isLoggedIn) {
    return $client->login();	
}


$user = null;
$accessLevel = null;
if ($isLoggedIn) {
	$user = $client->user();

	if ($user instanceof UserModel) {
		$professions = $user->getProfessions();
		if (count($professions) === 0) {
			$accessLevel = 0;
		} else {
			$accessLevel = $professions[0]['status'];
		}
	}
}

$smarty->assign('vars', [
	'is_logged_in' => $isLoggedIn,
    'user' => $user,
    'access_level' => $accessLevel,
]);

echo $this->ProcessTemplate('vars.tpl');

