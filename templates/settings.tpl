{form_start action='save_settings' id=$id}

<div class="pageoverflow">
    <p class="pageinput">
        <input type="submit" name="{$actionid}submit" value="{$mod->Lang('submit')}"/>
    </p>
</div>

<fieldset>
    <legend>{$mod->Lang('admin_settings_legend')}</legend>

    <p>{$mod->Lang('admin_settings_intro')}</p><br>

    <div class="pageoverflow">
        <p class="pagetext"><label for="enabled">{$mod->Lang('admin_settings_enabled')}:</label></p>
        <p class="pageinput">
            <input type="checkbox" id="enabled" name="{$actionid}enabled" value="1"
                   {if $enabled}checked="checked"{/if}/>
        </p>
    </div>

    <div class="pageoverflow">
        <p class="pagetext"><label for="domain">{$mod->Lang('admin_settings_domain')}:</label></p>
        <p class="pageinput">
            <input type="text" id="domain" name="{$actionid}domain" value="{$domain}">
        </p>
    </div>

    <div class="pageoverflow">
        <p class="pagetext"><label for="client_id">{$mod->Lang('admin_settings_client_id')}:</label></p>
        <p class="pageinput">
            <input type="text" id="client_id" name="{$actionid}client_id" value="{$client_id}">
        </p>
    </div>

    <div class="pageoverflow">
        <p class="pagetext"><label for="client_secret">{$mod->Lang('admin_settings_client_secret')}:</label></p>
        <p class="pageinput">
            <input type="text" id="client_secret" name="{$actionid}client_secret" value="{$client_secret}">
        </p>
    </div>

    <div class="pageoverflow">
        <p class="pagetext"><label for="unauthorized_page_id">{$mod->Lang('admin_settings_unauthorized_page_id')}:</label></p>
        <p class="pageinput">
            <input type="text" id="unauthorized_page_id" name="{$actionid}unauthorized_page_id" value="{$unauthorized_page_id}">
        </p>
    </div>

</fieldset>

{form_end}
