{SmitAuth action=vars}
{$enabledOnPage = "{content_module module='ECB2' field='checkbox' block='enabled_on_page' label='Enabled on page' tab='Smit Auth'}"}
{$requiredAccessLevel = "{content_module module='ECB2' field='dropdown' block='required_access_level' label='Required access level' values='0=0,1=1,2=2' tab='Smit Auth'}"}

{if $enabledOnPage === '1'}
    {SmitAuth required_access_level=$requiredAccessLevel}
{/if}
