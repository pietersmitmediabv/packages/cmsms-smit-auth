<?php
if (! function_exists("cmsms")) exit;
if (! $this->CheckPermission('Modify Site Preferences')) exit;

$enabled = $this->GetPreference('enabled', 0);
$domain = $this->GetPreference('domain');
$client_id = $this->GetPreference('client_id');
$client_secret = $this->GetPreference('client_secret');
$unauthorized_page_id = $this->getPreference('unauthorized_page_id');

$smarty->assign('enabled', $enabled);
$smarty->assign('domain', $domain);
$smarty->assign('client_id', $client_id);
$smarty->assign('client_secret', $client_secret);
$smarty->assign('unauthorized_page_id', $unauthorized_page_id);

echo $this->ProcessTemplate('settings.tpl');
