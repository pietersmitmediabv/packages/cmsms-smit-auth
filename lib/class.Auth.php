<?php

include_once __DIR__ . '/vendor/autoload.php';

class Auth
{
    /** @var \SMIT\SDK\Auth\Auth */
    public $client;

    protected $mod;

    public function __construct($mod = null)
    {
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		
        if ($mod === null) {
            $mod = cms_utils::get_module('SmitAuth');
        }

        $this->mod = $mod;

        $this->client = new SMIT\SDK\Auth\Auth($mod->getSdkConfig());
    }
}
