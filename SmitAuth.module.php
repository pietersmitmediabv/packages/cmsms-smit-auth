<?php

include_once __DIR__ . '/lib/class.Auth.php';

class SmitAuth extends CMSModule
{
    public function GetVersion()
    {
        return '0.1';
    }

    public function GetName()
    {
        return 'SmitAuth';
    }

    public function GetFriendlyName()
    {
        return 'SMIT. Auth';
    }

    public function GetAdminDescription()
    {
        return 'Authorize pages with SMIT Auth';
    }

    public function IsPluginModule()
    {
        return true;
    }

    public function GetAuthor()
    {
        return 'SMIT. Digitaal vakmanschap B.V.';
    }

    public function GetAuthorEmail()
    {
        return 'support@smit.net';
    }

    public function allowSmartyCaching()
    {
        return false;
    }

    public function VisibleToAdminUser()
    {
        return true;
    }

    public function HasAdmin()
    {
        return true;
    }

    public function GetAdminSection()
    {
        return 'content';
    }

    // @todo ECB2 toevoegen als dependency

    public function InitializeFrontend()
    {
        $this->RegisterModulePlugin();

        $this->SetParameterType('required_access_level', CLEAN_STRING);

        $routes[] = new CmsRoute('smitauth/login', $this->GetName(), ['action' => 'default'], true);
        $routes[] = new CmsRoute('smitauth/callback', $this->GetName(), ['action' => 'callback'], true);
		


        foreach ($routes as $route) {
            cms_route_manager::add_dynamic($route);
        }
    }

    public function InitializeAdmin()
    {
    }

    public function GetAdminMenuItems()
    {
        $items = [];

        if ($this->CheckPermission('Modify Site Preferences')) {
            $item = new CmsAdminMenuItem();
            $item->module = $this->GetName();
            $item->section = 'siteadmin';
            $item->title = 'Settings - SMIT Auth config';
            $item->description = 'Configure SMIT Auth environment variables';
            $item->action = 'admin_settings';
            $item->url = $this->create_url('m1_', $item->action);
            $items[] = $item;
        }

        return $items;
    }

    public function getSdkConfig()
    {
        $config = [
            'domain' => $this->GetPreference('domain'),
            'client_id' => $this->GetPreference('client_id'),
            'client_secret' => $this->GetPreference('client_secret'),
            'redirect_uri' => cms_config::get_instance()['root_url'] . '/smitauth/callback',
        ];

        $empty = array_filter($config, function ($item) {
            return $item == null;
        });

        if (count($empty) > 0) return null;

        return $config;
    }

    public static function page_type_lang_callback($str)
    {
        $mod = cms_utils::get_module('SmitAuth');
        if (is_object($mod)) return $mod->Lang('type_' . $str);
    }

    public static function template_help_callback($str)
    {
        // @todo add template help
        return '';
    }

    public static function reset_page_type_defaults(CmsLayoutTemplateType $type)
    {
        if ($type->get_originator() != 'SmitAuth') throw new CmsLogicException('Cannot reset contents for this template type');

        $fn = null;
        switch ($type->get_name()) {
            case 'authenticate':
                $fn = '_authenticate.tpl';
                break;
        }

        $fn = cms_join_path(__DIR__, 'templates', $fn);
        if (file_exists($fn)) return @file_get_contents($fn);
    }

}
