<?php

if (! defined('CMS_VERSION')) exit;

if (! $this->getSdkConfig()) {
    throw new \RuntimeException('SmitAuth config is incomplete');
}

if ($this->getPreference('enabled') !== '1') {
    return;
}

$client = (new Auth())->client;

if (! $client->isLoggedIn()) {
    return $client->login();	
}

$professions = $client->user()->getProfessions();

if (count($professions) === 0) {
    $accessLevel = 0;
} else {
    $accessLevel = $professions[0]['status'];
}

if ($params['required_access_level'] > $accessLevel) {
    return redirect_to_alias($this->getPreference('unauthorized_page_id'));
}
