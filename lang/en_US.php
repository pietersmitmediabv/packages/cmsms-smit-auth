<?php

$lang['type_SmitAuth'] = 'SmitAuth';
$lang['type_authenticate'] = 'Authenticate';

$lang['submit'] = 'Submit';
$lang['settings_updated'] = 'Settings were saved successfully!';

$lang['admin_settings_legend'] = 'Auth environment';
$lang['admin_settings_intro'] = 'These fields are to be filled with values from a SMIT Auth client. Don\'t edit these fields unless you know what you\'re doing.';
$lang['admin_settings_enabled'] = 'Enabled';
$lang['admin_settings_domain'] = 'Domain';
$lang['admin_settings_client_id'] = 'Client ID';
$lang['admin_settings_client_secret'] = 'Client secret';
$lang['admin_settings_unauthorized_page_id'] = 'Unauthorized page id';
