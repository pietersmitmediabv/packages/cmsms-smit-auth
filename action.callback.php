<?php

if (! defined('CMS_VERSION')) exit;

$client = (new Auth())->client;

return $client->callback();
