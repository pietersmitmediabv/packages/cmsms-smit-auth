<?php
if (! function_exists("cmsms")) exit;
if (! $this->CheckPermission('Modify Site Preferences')) exit;

$this->SetPreference('enabled', $params['enabled']);
$this->SetPreference('domain', $params['domain']);
$this->SetPreference('client_id', $params['client_id']);
$this->SetPreference('client_secret', $params['client_secret']);
$this->SetPreference('unauthorized_page_id', $params['unauthorized_page_id']);

$this->SetMessage($this->Lang('settings_updated'));
$this->RedirectToAdminTab('', null, 'admin_settings');
